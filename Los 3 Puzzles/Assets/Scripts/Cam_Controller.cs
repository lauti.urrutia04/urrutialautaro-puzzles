using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam_Controller : MonoBehaviour
{
    Vector2 mouseMirar;
    Vector2 suavidadV;
    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;
    GameObject jugador;
    public static Transform holdPos;

    void Start()
    {
        jugador = this.transform.parent.gameObject;
    }

    void Update()
    {
        Vector2 mouseDir = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        mouseDir = Vector2.Scale(mouseDir, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        suavidadV.x = Mathf.Lerp(suavidadV.x, mouseDir.x, 1f / suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, mouseDir.y, 1f / suavizado);

        mouseMirar += suavidadV;

        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -80f, 80f);

        transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, jugador.transform.up);
    }
}
