using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door_Controller : MonoBehaviour
{
    public static bool isTurnedOn = false;
    public float speed = 10;
    public float maxY;
    public float minY;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isTurnedOn)
        {
            if (transform.position.y <= maxY)
            {
                MoveUp();
            }
        }
        else
        {
            if (transform.position.y >= minY)
            {
                MoveDown();
            }
        }
    }

    public void MoveDown()
    {
        transform.position -= transform.up * speed * Time.deltaTime;
    }
    public void MoveUp()
    {
        transform.position += transform.up * speed * Time.deltaTime;
    }
}
