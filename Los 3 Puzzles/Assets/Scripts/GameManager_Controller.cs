using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager_Controller : MonoBehaviour
{
    public static bool colorsWon;
    public static bool gameWon = false;
    public static int grabbablesPos = 0;
    public List<Reciever_Controller> recievs;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckWin();
    }

    private void CheckWin()
    {
        //CheckGrabs();
        if (CheckGrabs())
        {
            gameWon = true;
        }
    }
    private bool CheckGrabs()
    {
        int i = 0;
        foreach (Reciever_Controller r in recievs)
        {
            if (r.colorOnTarget)
            {
                i++;
            }
        }
        if (i >= 3)
        {
            colorsWon = true;
            return true;
        }
        else
        {
            return false;
        }
    }
}
