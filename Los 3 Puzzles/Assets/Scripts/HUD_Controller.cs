using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HUD_Controller : MonoBehaviour
{
    public TMP_Text winText;
    void Start()
    {
        winText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager_Controller.gameWon)
        {
            Time.timeScale = 0;
            winText.text = "Ganaste!";
        }
    }
}
