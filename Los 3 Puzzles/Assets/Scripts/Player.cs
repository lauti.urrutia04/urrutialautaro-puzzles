using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    public Rigidbody rb;
    public Camera cam;
    public float speed;
    public float rayDist = 5;

    public TMP_Text interactuarTxt;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        interactuarTxt.text = "";
    }

    void Update()
    {
        Cursor.lockState = CursorLockMode.Locked;
        float FrontMove = Input.GetAxis("Vertical") * speed;
        float StrafeMove = Input.GetAxis("Horizontal") * speed;

        FrontMove *= Time.deltaTime;
        StrafeMove *= Time.deltaTime;
        transform.Translate(StrafeMove, 0, FrontMove);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (RaySwitch())
        {
            interactuarTxt.text = "'E' Interactuar";
            if (Input.GetKeyDown(KeyCode.E))
            {
                
                Door_Controller.isTurnedOn = !Door_Controller.isTurnedOn;
            }
        }
        else
        {
            interactuarTxt.text = "";
        }
        
    }

    public bool RaySwitch()
    {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.5f));
        Debug.DrawRay(ray.origin, ray.direction, Color.red);
        RaycastHit hitInfo;
        //return Physics.Raycast(ray, rayDist);
        if (Physics.Raycast(ray, out hitInfo, rayDist))
        {
            if (hitInfo.collider.GetComponent<Switch_Controller>() != null)
            {
                return true;
            }
            else { return false; }
        }
        else { return false; }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jump"))
        {
            this.rb.AddForce(new Vector3(rb.velocity.x, 20, rb.velocity.z), ForceMode.Impulse);
        }
    }
}
