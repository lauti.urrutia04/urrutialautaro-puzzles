using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reciever_Controller : MonoBehaviour
{
    public int colorNum;
    public bool colorOnTarget;

    void Start()
    {
        colorOnTarget = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obj"))
        {
            if (other.GetComponent<Grabable_Controller>().objNum == colorNum)
            {
                colorOnTarget = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Obj"))
        {
            if (other.GetComponent<Grabable_Controller>().objNum == colorNum)
            {
                colorOnTarget = false;
            }
        }
    }
}
